-- Common functions

local tremove = table.remove
local sqrt = math.sqrt

-- Partitioning

local quad = require("fizzx.quad")

local qinsert = quad.insert
local qremove = quad.remove
local qselect = quad.select
local qselectR = quad.selectRange

-- Collisions

local shape = require("fizzx.shapes")
local getBounds = shape.bounds

-- Internal data

-- list of shapes
local statics = {}
local dynamics = {}
local kinematics = {}

-- global gravity
local gravityx = 0
local gravityy = 0
-- maximum velocity limit of moving shapes
local maxVelocity = 1000
-- broad phase partitioning method
local partition = "none"
-- buffer reused in queries
local buffer = {}
-- some stats
local nchecks = 0

-- Internal functionality

-- returns shape index and its list
local function findShape(s)
  local t = s.list
  for i = 1, #t do
    if t[i] == s then
      return i, t
    end
  end
end

-- repartition moved or modified shapes
local function repartition(s)
  if partition == "quad" then
    -- reinsert in the quadtree
    local x, y, hw, hh = shape.bounds(s)
    qinsert(s, x, y, hw, hh)
  end
end

local function addShapeType(list, t, ...)
  assert(shape.new[t], "invalid shape type")
  local s = shape.new[t](...)
  s.list = list
  list[#list + 1] = s
  repartition(s)
  return s
end

-- changes the position of a shape
local function changePosition(a, dx, dy)
  shape.position(a, dx, dy)
  repartition(a)
end

-- resolves collisions
local function solveCollision(a, b, nx, ny, pen)
  -- shape a must be dynamic
  --assert(a.list == dynamics, "collision pair error")
  -- relative velocity
  local vx = a.xv - (b.xv or 0)
  local vy = a.yv - (b.yv or 0)
  -- penetration speed
  -- dot product of the velocity and collision normal
  local ps = vx*nx + vy*ny
  -- objects moving apart?
  --if ps < 0 then
  if ps <= 0 then
    -- coefficients
    -- restitution [1-2]
    local r = a.bounce
    local r2 = b.bounce
    if r2 and r2 > r then
      r = r2
    end
    r = r + 1
    -- r = r/(1/a.mass + 1/b.mass)
    -- friction [0-1]
    local f = a.friction
    local f2 = b.friction
    if f2 and f2 < f then
      f = f2
    end
    -- impulses
    -- penetration
    local px, py = nx*ps, ny*ps
    -- tangent
    local tx, ty = vx - px, vy - py
    -- integrate
    px, py = px*r, py*r
    tx, ty = tx*f, ty*f
 
    -- coulomb's law (optional)
    -- clamps the tangent impulse so that
    -- it doesn't exceed the separation impulse
    local p2 = px*px + px*px
    local t2 = tx*tx + ty*ty
    if p2 > 0 and t2 > p2 then
      --assert(t2 > 0)
      local d = 1/sqrt(t2)*sqrt(p2)
      tx, ty = tx*d, ty*d
    end

    local jx, jy = px + tx, py + ty
    -- mass
    local ma = a.mass
    if ma > 0 then
      ma = 1/ma
    end
    local mb = b.mass or 0
    if mb > 0 then
      mb = 1/mb
    end
    local mc = ma + mb
    --assert(mc > 0)
    if mc > 0 then
      ma = ma/mc
      mb = mb/mc
    end
    -- adjust the velocity of shape a
    a.xv = a.xv - jx*ma
    a.yv = a.yv - jy*ma
    if b.list == dynamics then
      -- adjust the velocity of shape b
      b.xv = b.xv + jx*mb
      b.yv = b.yv + jy*mb
    end
  end

----[[
  assert(pen >= 0, pen)
  -- positional correction
  local ca = pen*(b.correction or 0.5)
  local cb = -(pen - ca)
  if b.list == dynamics then
    -- max(pen - slop,0)/(A.inv_mass + B.inv_mass)*percent
    changePosition(b, nx*cb, ny*cb)
  end
  pen = ca
 -- ]]

  -- separation
  local sx, sy = nx*pen, ny*pen
  --assert(sx ~= 0 or sy ~= 0, "collision separation error")
  -- store the separation for shape a
  a.sx = a.sx + sx
  a.sy = a.sy + sy
  -- separate the pair by moving shape a
  changePosition(a, sx, sy)
end

-- check and report collisions
local function collision(a, b, dt)
  -- track the number of collision checks (optional)
  nchecks = nchecks + 1
  local nx, ny, pen = shape.test(a, b, dt)
  if pen == nil then
    return
  end
  --assert(pen > 0, "collision penetration error")
  -- collision callbacks
  local ra = true
  local rb = true
  if a.onCollide then
    ra = a:onCollide(b, nx, ny, pen)
  end
  if b.onCollide then
    rb = b:onCollide(a, -nx, -ny, pen)
  end
  -- ignore collision if either callback returned false
  if ra == true and rb == true then
    solveCollision(a, b, nx, ny, pen)
  end
end

-- Public functionality

local fizz = {}

-- updates the simulation
function fizz.update(dt)
  -- track the number of collision checks (optional)
  nchecks = 0

  -- todo: user defined velocities change
  -- as we add gravity before resolving collisions
  
  -- update velocity vectors
  local xg = gravityx*dt
  local yg = gravityy*dt
  local mv2 = maxVelocity*maxVelocity
  for i = 1, #dynamics do
    local d = dynamics[i]
    -- damping
    local c = 1 + d.damping*dt
    local xv = d.xv/c
    local yv = d.yv/c
    -- gravity
    local g = d.gravity or 1
    xv = xv + xg*g
    yv = yv + yg*g
    -- threshold
    local v2 = xv*xv + yv*yv
    if v2 > mv2 then
      local n = maxVelocity/sqrt(v2)
      xv = xv*n
      yv = yv*n
    end
    d.xv = xv
    d.yv = yv
    -- reset separation
    d.sx = 0
    d.sy = 0
  end
  
  -- move kinematic shapes
  for i = 1, #kinematics do
    local k = kinematics[i]
    changePosition(k, k.xv*dt, k.yv*dt)
  end
  -- move dynamic shapes
  if partition == 'quad' then
    -- quadtree partitioning
    for i = 1, #dynamics do
      local d = dynamics[i]
      -- move to new position
      changePosition(d, d.xv*dt, d.yv*dt)
      -- check and resolve collisions
      -- query for potentially colliding shapes
      --qselect(d, buffer)
      local x, y, hw, hh = getBounds(d)
      qselectR(x, y, hw, hh, buffer)
      -- note: we check each collision pair twice
      for j = #buffer, 1, -1 do
        local d2 = buffer[j]
        if d2 ~= d then
          collision(d, d2, dt)
        end
        -- clear the buffer during iteration
        buffer[j] = nil
      end
    end
    quad.prune()
  else
    -- brute force
    for i = 1, #dynamics do
      local d = dynamics[i]
      -- move to new position
      changePosition(d, d.xv*dt, d.yv*dt)
      -- check and resolve collisions
      for j = 1, #statics do
        collision(d, statics[j], dt)
      end
      for j = 1, #kinematics do
        collision(d, kinematics[j], dt)
      end
      -- note: we check each collision pair only once
      for j = i + 1, #dynamics do
        collision(d, dynamics[j], dt)
      end
    end
  end
end

-- gets the global gravity
function fizz.getGravity()
  return gravityx, gravityy
end

-- sets the global gravity
function fizz.setGravity(x, y)
  gravityx, gravityy = x, y
end

-- static shapes do not move or respond to collisions
function fizz.addStatic(shape, ...)
  return addShapeType(statics, shape, ...)
end

-- kinematic shapes move only when assigned a velocity
function fizz.addKinematic(shape, ...)
  local s = addShapeType(kinematics, shape, ...)
  s.xv, s.yv = 0, 0
  return s
end

-- dynamic shapes are affected by gravity and collisions
function fizz.addDynamic(shape, ...)
  local s = addShapeType(dynamics, shape, ...)
  s.friction = 1
  s.bounce = 0
  s.damping = 0
  s.xv, s.yv = 0, 0
  s.sx, s.sy = 0, 0
  s.mass = 1
  return s
end

-- adjusts mass
function fizz.setDensity(s, d)
  s.mass = shape.area(s)*d
end

-- removes shape from its list
function fizz.removeShape(s)
  local i, t = findShape(s)
  if i then
    s.list = nil
    tremove(t, i)
    qremove(s)
  end
end

-- gets the position of a shape (starting point for line shapes)
function fizz.getPosition(a)
  return a.x, a.y
end

-- sets the position of a shape
function fizz.setPosition(a, x, y)
  changePosition(a, x - a.x, y - a.y)
end

-- gets the velocity of a shape
function fizz.getVelocity(a)
  return a.xv or 0, a.yv or 0
end

-- sets the velocity of a shape
function fizz.setVelocity(a, xv, yv)
  a.xv = xv
  a.yv = yv
end

-- gets the separation of a shape for the last frame
function fizz.getDisplacement(a)
  return a.sx or 0, a.sy or 0
end

-- sets the partitioning method
function fizz.setPartition(p)
  assert(p == "quad" or p == "none", "invalid partitioning method")
  partition = p
end

-- gets the partitioning method
function fizz.getPartition()
  return partition
end

-- gets the number of collision checks for the last frame
function fizz.getCollisionCount()
  return nchecks
end

-- Public access to some tables

fizz.repartition = repartition
fizz.statics = statics
fizz.dynamics = dynamics
fizz.kinematics = kinematics

return fizz