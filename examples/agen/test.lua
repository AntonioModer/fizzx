display:create("Fizz", 800, 600, 32, true)

fizz = require("fizzx.fizz")

--fizz.partition = "none"
fizz.setPartition("quad")

terminal = require("utils.log.terminal")

--quad.mincellsize = 16

fizz.addStatic("circle", -300, 200, 50)
fizz.addStatic("rect", 0, 200, 100, 50)
fizz.addStatic("rect", 300, 200, 25, 75)

fizz.addStatic("line", -300, 50, -300, -50)
fizz.addStatic("line", -150, 0, -50, 0)
fizz.addStatic("line", 100, -50, 100, 50)
fizz.addStatic("line", 350, 0, 250, 0)
fizz.addStatic("line", -300, -250, -250, -200)
fizz.addStatic("line", -150, -150, -100, -200)
fizz.addStatic("line", 150, -250, 100, -200)
fizz.addStatic("line", 250, -200, 300, -150)

drect = fizz.addDynamic("rect", 0, 0, 50, 30)
drect.r = 30
fizz.setVelocity(drect, 100, 0)

sprite = Sprite()
display.viewport:add_child(sprite)
canvas = sprite.canvas

snap = false

local function createStatic(shape, ...)
  local s = fizz.addStatic(...)
  table.insert(shapes, s)
end

local function drawShape(shape, ox, oy)
  ox, oy = ox or 0, oy or 0
  if shape.shape == "rect" then
    canvas:move_to(shape.x + ox, shape.y + oy)
    canvas:rectangle(shape.hw*2, shape.hh*2)
  elseif shape.shape == "circle" then
    canvas:move_to(shape.x + ox, shape.y + oy)
    canvas:circle(shape.r)
  elseif shape.shape == "line" then
    canvas:move_to(shape.x + ox, shape.y + oy)
    canvas:circle(5)
    canvas:line_to(shape.x2 + ox, shape.y2 + oy)
  else
    assert(false, shape.shape or 'nil')
  end
  canvas:set_line_style(1, WHITE, 1)
  canvas:stroke()
  local xv, yv = fizz.getVelocity(shape)
  if xv and yv then
    canvas:move_to(shape.x + ox, shape.y + oy)
    canvas:rel_line_to(xv, yv)
    canvas:set_line_style(1, LIME, 1)
    canvas:stroke()
  end
  if shape.dvx and shape.dvy then
    canvas:move_to(shape.x + ox, shape.y + oy)
    canvas:rel_line_to(-shape.dvx, -shape.dvy)
    canvas:set_line_style(1, RED, 1)
    canvas:stroke()
  end
end

local function redrawShapes()
  canvas:clear()
  for i, v in ipairs(fizz.statics) do
    drawShape(v)
  end
  for i, v in ipairs(fizz.dynamics) do
    drawShape(v)
  end
  drawShape(drect, drect.nx*drect.pen, drect.ny*drect.pen)
end

drect.onCollide = function(drect, b, nx, ny, pen)
  drect.nx = nx
  drect.ny = ny
  drect.pen = pen
  drect.collision = true
  --return true
end

timer = Timer()
timer:start(16, true)
timer.on_tick = function(timer)
  drect.nx = 0
  drect.ny = 0
  drect.pen = 0
  drect.collision = false
  drect.x = mouse.xaxis
  drect.y = mouse.yaxis
  if snap then
    drect.x = math.floor(drect.x/10)*10
    drect.y = math.floor(drect.y/10)*10
  end
  fizz.repartition(drect)

  drect.dvx, drect.dvy = 0, 0
  
  fizz.update(0.1)

  redrawShapes()
  
  terminal.trace("collision", "false")
  if drect.collision == true then
    terminal.trace("collision", "true")
  end
  terminal.trace("nx", drect.nx)
  terminal.trace("ny", drect.ny)
  terminal.trace("pen", drect.pen)
  terminal.trace("part", fizz.partition)
  terminal.trace("cchecks", fizz.cchecks)
  terminal.trace("snap", snap)
  terminal.trace("friction", drect.friction)
  terminal.trace("bounce", drect.bounce)
  
  --[[
  local r = {}
  fizz.queryPoint(r, mouse.xaxis, mouse.yaxis)
  for i, v in ipairs(r) do
    canvas:move_to(v.x, v.y)
    canvas:circle(5)
    canvas:fill()
  end
  ]]
end

mouse.on_wheelmove = function(mouse, z)
  local d = 1
  if z < 0 then
    d = -d
  end
  local x, y = fizz.getVelocity(drect)
  local a = math.rad(d*10)
  local c = math.cos(a)
  local s = math.sin(a)
  local tx = c*x - s*y
  local ty = s*x + c*y
  fizz.setVelocity(drect, tx, ty)
end

mouse.on_press = function(mouse)
  if drect.shape == 'circle' then
    drect.shape = 'rect'
  elseif drect.shape == 'rect' then
    drect.shape = 'circle'
  end
end

function keyboard:on_press(k)
  if k == KEY_P then
    if fizz.partition == 'quad' then
      fizz.partition = "none"
    else
      fizz.partition = "quad"
    end
  elseif k == KEY_G then
    snap = not snap
  elseif k == KEY_UP then
    drect.friction = drect.friction + 0.1
    drect.friction = math.min(drect.friction, 1)
  elseif k == KEY_DOWN then
    drect.friction = drect.friction - 0.1
    drect.friction = math.max(drect.friction, 0)
  elseif k == KEY_LEFT then
    drect.bounce = drect.bounce + 0.1
    drect.bounce = math.min(drect.bounce, 1)
  elseif k == KEY_RIGHT then
    drect.bounce = drect.bounce - 0.1
    drect.bounce = math.max(drect.bounce, 0)
  end
end