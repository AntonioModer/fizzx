display:create("FizzQ", 800, 600, 32, true)

quad = require("fizzx.quad")

--fizz.partition = "none"

terminal = require("utils.log.terminal")

camera = display.viewport.camera
layer = Layer()
sprite = Sprite()
layer:add_child(sprite)
layer:add_child(camera)
canvas = sprite.canvas

snap = true

shapes = {}

local function createRect(x1, y1, x2, y2)
  local cx, cy = (x1 + x2)/2, (y1 + y2)/2
  local hw, hh = math.abs(x1 - x2)/2, math.abs(y1 - y2)/2
  local s = { shape = "rect", x = cx, y = cy, hw = hw, hh = hh }
  table.insert(shapes, s)
  quad.insert(s, cx, cy, hw, hh)
end

local function drawShape(shape, c)
  if shape.shape == "rect" then
    canvas:move_to(shape.x, shape.y)
    canvas:rectangle(shape.hw*2, shape.hh*2)
  elseif shape.shape == "circle" then
    canvas:move_to(shape.x, shape.yx)
    canvas:circle(shape.r)
  elseif shape.shape == "line" then
    canvas:move_to(shape.xx, shape.y)
    canvas:circle(5)
    canvas:line_to(shape.x2, shape.y2x)
  else
    assert(false, shape.shape or 'nil')
  end
  canvas:set_line_style(1, c, 1)
  canvas:stroke()
  local x2, y2, hw2, hh2 = quad.getRange(shape)
  if x2 and y2 then
    canvas:move_to(x2, y2)
    canvas:rectangle(hw2*2, hh2*2)
    canvas:set_line_style(1, c, 0.25)
    canvas:stroke()
  end
end

local function redrawShapes()
  canvas:clear()
  for i, v in ipairs(shapes) do
    drawShape(v, WHITE)
  end
end

timer = Timer()
timer:start(16, true)
function timer:on_tick()
  redrawShapes()
  if x1 == nil or y1 == nil then
    return
  end
  local x2, y2 = mouse.xaxis, mouse.yaxis
  x2, y2 = camera:get_world_point(x2, y2)
  if snap then
    x1 = math.floor(x1/10)*10
    y1 = math.floor(y1/10)*10
    x2 = math.floor(x2/10)*10
    y2 = math.floor(y2/10)*10
  end
  canvas:move_to(x1, y1)
  canvas:line_to(x2, y1)
  canvas:line_to(x2, y2)
  canvas:line_to(x1, y2)
  canvas:close_path()
  canvas:stroke()
  if keyboard:is_down(KEY_LSHIFT) then
    local cx, cy = (x1 + x2)/2, (y1 + y2)/2
    local hw, hh = math.abs(x1 - x2)/2, math.abs(y1 - y2)/2
    q = quad.selectRange(cx, cy, hw, hh)
    for i, v in ipairs(q) do
      drawShape(v, RED)
    end
  end
end

function mouse:on_wheelmove(z)
  local d = 1
  if z < 0 then
    d = -d
  end
end

function mouse:on_move(dx, dy)
  if mouse:is_down(MBUTTON_RIGHT) then
    camera:change_position(-dx, -dy)
  end
end

function mouse:on_press(b)
  if b == MBUTTON_RIGHT then
    return
  end
  x1, y1 = mouse.xaxis, mouse.yaxis
  x1, y1 = camera:get_world_point(x1, y1)
  if snap then
    x1 = math.floor(x1/10)*10
    y1 = math.floor(y1/10)*10
  end
end

function mouse:on_release(b)
  if b == MBUTTON_RIGHT then
    return
  end
  if keyboard:is_down(KEY_LSHIFT) then
    x1, y1 = nil, nil
    return
  end
  local x2, y2 = mouse.xaxis, mouse.yaxis
  x2, y2 = camera:get_world_point(x2, y2)
  if snap then
    x2 = math.floor(x2/10)*10
    y2 = math.floor(y2/10)*10
  end
  createRect(x1, y1, x2, y2)
  x1, y1 = nil, nil
end